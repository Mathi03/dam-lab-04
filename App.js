import React, {Component} from 'react';
import {View, Text, StyleSheet, TextInput} from 'react-native';

import Message from './app/components/message/Message';
import Body from './app/components/body/Body';
import OurFlatList from './app/components/ourFlatList/ourFlatList';
//import ConexionFetch from './app/components/conexionFetch/ConexionFetch';

const ConexionFetch = require('./app/components/conexionFetch/ConexionFetch');

const provincias = [
  {
    id: 1,
    name: 'Arequipa',
  },
  {
    id: 2,
    name: 'Puno',
  },
  {
    id: 3,
    name: 'Cuzco',
  },
];

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      textValue: '',
      count: 0,
    };
  }

  changeTextInput = textoInsertado => {
    console.log(textoInsertado);

    this.setState({textValue: textoInsertado});
  };
  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return <ConexionFetch />;
  }

  /*
  render() {
    return (
      <View style={styles.container}>
        <OurFlatList />
      </View>
    );
  }*/

  /*
  render() {
    return (
      <View style={styles.container}>
        <Message />
        <View style={styles.text}>
          <Text> Ingrese su edad</Text>
        </View>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={texto => this.changeTextInput(texto)}
          value={this.state.textValue}
        />

        <Body textBody={'Texto en Body'} onBodyPress={this.onPress} />

        <View style={styles.countContainer}>
          <Text style={styles.countText}>{this.state.count}</Text>
        </View>
        {provincias.map(item => (
          <View>
            <Text>{item.name}</Text>
          </View>
        ))}
      </View>
    );
  }*/
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignContent: 'center',
    justifyContent: 'center',
    padding: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
});

export default App;
